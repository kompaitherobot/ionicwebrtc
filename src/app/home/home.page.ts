import { Component, OnInit, ElementRef } from '@angular/core';
import { WebrtcService} from '../providers/webrtc.service';
import { Webrtc2Service } from '../providers/webrtc2.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  topVideoFrame = 'partner-video';
  userId: string;
  partnerId: string;
  myEl: HTMLMediaElement;
  partnerEl: HTMLMediaElement;
  partnerEl2: HTMLMediaElement;

  constructor(
    public webRTC: WebrtcService,
    public webRTC2: Webrtc2Service,
    public elRef: ElementRef
  ) {}

  ngOnInit(): void {
    this.myEl = this.elRef.nativeElement.querySelector('#my-video');
    this.partnerEl = this.elRef.nativeElement.querySelector('#partner-video');
    this.partnerEl2 = this.elRef.nativeElement.querySelector('#partner2-video');
  }

  login() {
    this.webRTC.init(this.userId, this.myEl, this.partnerEl);
    this.webRTC2.init(this.userId + "2", this.myEl, this.partnerEl2);
  }

  call() {
    this.webRTC.call(this.partnerId);
    this.webRTC2.call(this.partnerId+"2");
    //this.swapVideo('my-video');
  }

  swapVideo(topVideo: string) {
    this.topVideoFrame = topVideo;
  }
}
