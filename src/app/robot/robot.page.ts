import { Component, OnInit, ElementRef } from '@angular/core';
import { WebrtcService } from '../providers/webrtc.service';
import { Webrtc2Service } from '../providers/webrtc2.service';

@Component({
  selector: 'app-robot',
  templateUrl: './robot.page.html',
  styleUrls: ['./robot.page.scss'],
})
export class RobotPage implements OnInit {

  topVideoFrame = 'partner-video';
  userId: string;
  partnerId: string;
  myEl: HTMLMediaElement;
  myEl2: HTMLMediaElement;
  partnerEl: HTMLMediaElement;

  constructor(
    public webRTC: WebrtcService,
    public webRTC2: Webrtc2Service,
    public elRef: ElementRef
  ) {}

  ngOnInit(): void {
    this.myEl = this.elRef.nativeElement.querySelector('#my-video');
    this.myEl2 = this.elRef.nativeElement.querySelector('#my-video2');
    this.partnerEl = this.elRef.nativeElement.querySelector('#partner-video');
  }

  login() {
    this.webRTC.init(this.userId, this.myEl, this.partnerEl);
    this.webRTC2.init(this.userId + "2" ,this.myEl2, this.partnerEl);
  }

  call() {
    this.webRTC.call(this.partnerId);
    this.webRTC2.call(this.partnerId + "2");
    //this.swapVideo('my-video');
  }

  swapVideo(topVideo: string) {
    this.topVideoFrame = topVideo;
  }

}
