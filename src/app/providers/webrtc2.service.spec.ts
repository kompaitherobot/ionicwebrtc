import { TestBed } from '@angular/core/testing';

import { Webrtc2Service } from './webrtc2.service';

describe('Webrtc2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Webrtc2Service = TestBed.get(Webrtc2Service);
    expect(service).toBeTruthy();
  });
});
